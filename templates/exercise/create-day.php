<form id="dayCreate" action="api/day/create.php">

    <div class="form-group">
        <label for="day">Day</label>
        <input type="text" class="form-control" id="day" placeholder="Day">
    </div>

    <div id="exerciseDiv">
    </div>

    <input type="hidden" name="plan_id" value="" id="plan_id" />
    <button type="submit" class="btn btn-primary">Create Day</button>
    <button type="button" id="exerciseForm" class="btn btn-primary">Add Exercise</button>
    <button type="button" id="back" class="btn btn-secondary">Back</button>

</form>