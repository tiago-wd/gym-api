<form id="userCreate" action="api/user/create.php">
    <div class="form-group">
        <label for="first_name">First Name</label>
        <input type="text" class="form-control" id="first_name" placeholder="First Name">
    </div>
    <div class="form-group">
        <label for="last_name">Last Name</label>
        <input type="text" class="form-control" id="last_name" placeholder="Last Name">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" placeholder="Email">
    </div>
    <button type="submit" id="submitUser" class="btn btn-primary">Create User</button>
    <button type="button" id="back" class="btn btn-secondary">Back</button>
</form>