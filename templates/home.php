<div class="row">
    <div class="col-sm-12">
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Plan Name</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody id="plans">
        </tbody>
    </table>
    </div>
</div>
<div class="row mt-5">
    <div class="col-sm-12">
        <button type="button" id="planForm" class="btn btn-primary btn-lg btn-block">Add Plan</button>
        <button type="button" id="userList" class="btn btn-secondary btn-lg btn-block">List Users</button>
    </div>
</div>