<form id="planCreate">

    <div class="form-group">
        <label for="plan_name">Plan</label>
        <input type="text" class="form-control" id="plan_name" placeholder="Plan Name">
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="users">Users</label>
        </div>
        <select class="custom-select" id="user_id" multiple>
        </select>
    </div>

    <div id="days" class="row">
    </div>
    
    <button type="submit" id="submitPlan" class="btn btn-primary">Create Plan</button>
    <button type="button" id="dayForm" class="btn btn-primary">Add Exercise</button>
    <button type="button" id="back" class="btn btn-secondary">Back</button>

</form>

    