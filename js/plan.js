$(document).on('click', "#planForm", function() {  
    $.ajax({
        type: "GET",
        url: "api/user/read.php",
        success: function(data) {
            var users;
            $.each(data, function(index, user) {
                users += '<option value="'+user.id+'">'+user.first_name + ' ' + user.last_name+'</option>';
            });
            
            $("#root").load("templates/plan/create.php", function() {
                $("#planCreate").attr('action', 'api/plan/create.php');
                $("#user_id").html(users);
            });
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });
});

$(document).on('click', "#planEditForm", function() {
    var plan_id = $(this).attr("plan_id");
    loadPlan(plan_id);
});

$(document).on('click', "#planDelete", function() {
    var plan_id = $(this).attr("plan_id");
    
    $.ajax({
        type: "GET",
        url: "api/plan/delete.php?id="+plan_id,
        success: function(data) {           
            $("#messageSuccess").text(data.message);
            $("#success").show();
            loadHome();
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });

});

$(document).on('submit', "#planCreate", function(e) {
    e.preventDefault();
    
    var form = $(this);
    var url = form.attr('action');
    var data = {
        plan_name: $("#plan_name").val(),
        user_id: $("#user_id").val(),
    }

    data.day = $("input[id='day']").map(function() {
        return $(this).val();
    }).get();
    data.exercise = $("input[id='exercise']").map(function() {
        return $(this).val();
    }).get();

    submitForm(url, data);

});

$(document).on('submit', "#dayCreate", function(e) {
    e.preventDefault();
    
    var plan_id = $("#plan_id").val();
    var form = $(this);
    var url = form.attr('action');
    var data = {
        name: $("#day").val(),
        plan_id: plan_id
    }

    data.exercises = $("input[id='exercise']").map(function() {
        return $(this).val();
    }).get();

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
            $("#messageSuccess").text(data.message);
            $("#success").show();
            loadPlan(plan_id)
        },
        error: function(error) {           
            $("#messageDanger").text(error.responseJSON.message);
            $("#danger").show();
        }
    });

});

$(document).on('click', "#dayEditForm", function() {
    var exercise_id = $(this).attr("exercise_id");
    $("#root").load("templates/exercise/create-day.php", function() {        
        $("#plan_id").val(plan_id);
    })
});

$(document).on('click', "#dayForm", function() {
    var plan_id = $(this).attr("plan_id");
    $("#root").load("templates/exercise/create-day.php", function() {        
        $("#plan_id").val(plan_id);
    })
});

$(document).on('click', "#exerciseForm", function() {
    $("#exerciseDiv").append($('<div>').load("templates/exercise/create.php"));
});

$(document).on('click', "#exerciseDelete", function(e) {
    var exercise_id = $(this).attr("exercise_id");
    
    $.ajax({
        type: "GET",
        url: "api/exercise/delete.php?id="+exercise_id,
        success: function(data) {           
            $("#messageSuccess").text(data.message);
            $("#success").show();
            $(e.target).parent().remove();
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });

});

$(document).on('click', "#addExerciseInput", function() {
    var day_id = $(this).attr("day_id");
    
    var data = {
        name: $("#exercise_" + day_id).val(),
        day_id: day_id
    }

    $.ajax({
        type: "POST",
        url: "api/exercise/create.php",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
            $("#messageSuccess").text(data.message);
            $("#success").show();
            $.ajax({
                type: "GET",
                url: "api/day/read.php?id=" + day_id,
                success: function(data) {
                    var exercise = '';
                    if(data.exercises) {
                        $.each(data.exercises, function(index, ex) {
                            exercise += '<li class="list-group-item">' + ex.name + '<small id="exerciseDelete" exercise_id="' + ex.id + '" class="float-right pointer">Delete</small></li>';
                        });                       
                    }
        
                    var day = '<div class="card">' +
                            '<div class="card-header">' +
                                data.name + 
                            '</div>' + 
                            '<ul class="list-group list-group-flush">' +
                            exercise +
                            '</ul>' +
                            '<div class="input-group" id="exerciseInput_' + day_id + '">' +
                            '<input type="text" class="form-control" id="exercise_' + day_id + '" name="exercise[]" placeholder="Exercise"><button day_id="' + day_id + '" id="addExerciseInput" type="button" class="btn btn-primary btn-sm">Add</button>' +
                            '</div>'+
                        '</div>';
                    $("#day_" + day_id).html(day);
                },
                error: function(error) {
                    $("#message").text(error.responseJSON.message);
                    $("#alert").show();
                }
            });
        },
        error: function(error) {           
            $("#messageDanger").text(error.responseJSON.message);
            $("#danger").show();
        }
    });
});