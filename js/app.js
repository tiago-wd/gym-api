$(document).ready(function() {
    loadHome();
});

$(document).on('click', "#home", function() {     
    loadHome();
});

$(document).on('click', "#back", function() {
    loadHome();
});

function loadHome() {
    $.ajax({
        type: "GET",
        url: "api/plan/read.php",
        success: function(data) {
            var plans = '';
            $.each(data, function(index, plan) {
                plans += '<tr>'+
                            '<th scope="row">'+index+'</th>'+
                            '<td>'+plan.plan_name+'</td>'+
                            '<td>'+
                            '<button id="planEditForm" plan_id="'+plan.id+'" type="button" class="btn btn-primary btn-sm mr-2">Edit</button>'+
                            '<button id="planDelete" plan_id="'+plan.id+'" type="button" class="btn btn-danger btn-sm">Delete</button>'+
                            '</td><tr>';
            });
            
            $("#root").load("templates/home.php", function() {
                $("#plans").html(plans);
            });
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });
}

function loadPlan(plan_id) {
    $.ajax({
        type: "GET",
        url: "api/plan/read.php?id="+plan_id,
        success: function(plan) {           
            $.ajax({
                type: "GET",
                url: "api/user/read.php",
                success: function(users) {
                    var users = loadUsers(users, plan);
                    var days = loadDays(plan.days);
                   
                    $("#root").load("templates/plan/create.php", function() {
                        $("#planCreate").attr('action', 'api/plan/edit.php?id='+plan.id);
                        $("#submitPlan").html("Edit Plan");
                        $("#dayForm").attr("plan_id", plan.id);
                        $("#dayForm").show();
                        $("#plan_name").val(plan.plan_name);
                        $("#user_id").html(users);
                        $("#days").html(days);
                    });
                },
                error: function(error) {           
                    $("#message").text(error.responseJSON.message);
                    $("#alert").show();
                }
            });
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });
}

function loadUser(user_id) {
    $.ajax({
        type: "GET",
        url: "api/user/read.php?id=" + user_id,
        success: function(user) {           
            $("#root").load("templates/user/create.php", function() {
                $("#userCreate").attr('action', 'api/user/edit.php?id=' + user.id);
                $("#submitUser").html("Edit User");
                $("#first_name").val(user.first_name);
                $("#last_name").val(user.last_name);
                $("#email").val(user.email);
            });
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });
}

function loadDays(plan_days) {
    var days = '';

    $.each(plan_days, function(index, day) {
        var exercise = '';
        if(day.exercises) {
            $.each(day.exercises, function(index, ex) {
                exercise += '<li class="list-group-item">' + ex.name + '<small id="exerciseDelete" exercise_id="' + ex.id + '" class="float-right pointer">Delete</small></li>';
            });                       
        }
        
        days += '<div class="col-sm-6 mb-4" id="day_' + index + '">' + 
                '<div class="card">' +
                    '<div class="card-header">' +
                        day.day + 
                    '</div>' + 
                    '<ul class="list-group list-group-flush">' +
                    exercise +
                    '</ul>' +
                    '<div class="input-group" id="exerciseInput_' + index + '">' +
                    '<input type="text" class="form-control" id="exercise_' + index + '" name="exercise[]" placeholder="Exercise"><button day_id="' + index + '" id="addExerciseInput" type="button" class="btn btn-primary btn-sm">Add</button>' +
                    '</div>'+
                '</div>' +
            '</div';
    })

    return days;
}

function loadUsers(usersData, plan) {
    var users = '';
    $.each(usersData, function(index, user) {
        var selected = '';
        $.each(plan.users, function(index, plan_user) {
            if(plan_user.id == user.id) {
                selected = 'selected';
            }
        })
        users += '<option ' + selected + ' value="'+user.id+'">'+user.first_name + ' ' + user.last_name+'</option>';
    });

    return users;
}

function listUsers() {
    $.ajax({
        type: "GET",
        url: "api/user/read.php",
        success: function(data) {
            var users = '';
            $.each(data, function(index, user) {
                users += '<tr>' +
                            '<th scope="row">' + index + '</th>' +
                            '<td>' + user.first_name + ' ' + user.last_name + '</td>' +
                            '<td>' + user.email + '</td>' +
                            '<td>' +
                            '<button id="userEditForm" user_id="' + user.id + '" type="button" class="btn btn-primary btn-sm mr-2">Edit</button>' +
                            '<button id="userDelete" user_id="' + user.id + '" type="button" class="btn btn-danger btn-sm">Delete</button>' +
                            '</td><tr>';
            });
            
            $("#root").load("templates/user/list.php", function() {
                $("#users").html(users);
            });
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });
}

function submitForm(url, data) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
            $("#messageSuccess").text(data.message);
            $("#success").show();
            loadHome();
        },
        error: function(error) {           
            $("#messageDanger").text(error.responseJSON.message);
            $("#danger").show();
        }
    });
}