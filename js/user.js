$(document).on('click', "#userList", function() {     
    listUsers();
});

$(document).on('click', "#userForm", function() {  
    $("#root").load("templates/user/create.php");
});

$(document).on('click', "#userDelete", function() {
    var user_id = $(this).attr("user_id");
    
    $.ajax({
        type: "GET",
        url: "api/user/delete.php?id="+user_id,
        success: function(data) {           
            $("#messageSuccess").text(data.message);
            $("#success").show();
            listUsers();
        },
        error: function(error) {           
            $("#message").text(error.responseJSON.message);
            $("#alert").show();
        }
    });

});

$(document).on('submit', "#userCreate", function(e) {
    e.preventDefault();
    
    var form = $(this);
    var url = form.attr('action');
    var data = {
        first_name: $("#first_name").val(),
        last_name: $("#last_name").val(),
        email: $("#email").val(),
    }

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
            $("#messageSuccess").text(data.message);
            $("#success").show();
            listUsers();
        },
        error: function(error) {           
            $("#messageDanger").text(error.responseJSON.message);
            $("#danger").show();
        }
    });
});

$(document).on('click', "#userEditForm", function() {
    var user_id = $(this).attr("user_id");
    loadUser(user_id);
});