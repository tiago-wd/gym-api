<?php

include_once 'Base.class.php';

class Exercise extends Base
{
    public function __construct($db, $table)
    {
        parent::__construct($db, $table);
    }

    public function create($params)
    {   
        parent::create($params);
        return $this->listExercises();
    }

    public function listExercises()
    {
        return parent::list();
    }

    public function findExercise($id)
    {
        return parent::find($id);
    }

    public function edit($user_id, $params) 
    {
        return parent::edit($user_id, $params);
    }

    public function delete($id) 
    {
        return parent::delete($id);
    }
}