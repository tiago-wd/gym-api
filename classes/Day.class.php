<?php

include_once 'Base.class.php';
include_once 'Exercise.class.php';

class Day extends Base
{
    private $exercise;

    public function __construct($db, $table)
    {
        parent::__construct($db, $table);
        $this->exercise = new Exercise($db, 'exercises');
    }

    public function create($params)
    {   
        $paramsDay = [
            'name' => $params->name,
            'plan_id' => $params->plan_id
        ];
        $day = parent::create((object) $paramsDay);
        $day_id = $this->getInsertedId();
        if(!empty($params->exercises) && $day) {
            foreach($params->exercises as $exercise) {
                $exercise = [
                    'day_id' => $day_id,
                    'name' => $exercise
                ];
                $this->exercise->create((object) $exercise);
            }
        }
        return $day;
    }

    public function listDays()
    {
        return parent::list();
    }

    public function findDay($id)
    {
        $day = parent::find($id);
        $day['exercises'] = $this->getDayExercises($id);
        return $day;
    }

    public function getDayExercises($day_id)
    {
        $query = "SELECT * FROM exercises WHERE day_id = :day_id";
        $bindParamss['day_id'] = $day_id;
        
        $stmt = $this->connection->prepare($query);
        $stmt->execute($bindParamss);
        
        $count = $stmt->rowCount();
        $exercises = [];
        if($count > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                if($id) {
                    $exercises[] = [
                        "id" => $id,
                        "name" => $name
                    ];
                }
            }
        }
        return $exercises;
    }

    public function edit($day_id, $params) 
    {
        return parent::edit($day_id, $params);
    }

    public function delete($id) 
    {
        return parent::delete($id);
    }
}