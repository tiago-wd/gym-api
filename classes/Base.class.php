<?php

class Base
{
    protected $connection;
    protected $table;
 
    public function __construct($db, $table)
    {
        $this->connection = $db;
        $this->table = $table;
    }

    public function create($params)
    {   
        $fields = '';
        $values = '';
        foreach($params as $key => $param) {
            $fields .= " {$key},";
            $values .= ":{$key},";
            $params->$key = htmlspecialchars(strip_tags($param));
            $bindParams[$key] = $param;
        }

        $fields = substr($fields, 0, -1);
        $values = substr($values, 0, -1);

        $query = "INSERT INTO {$this->table} ({$fields}) VALUES ({$values})";
        $stmt = $this->connection->prepare($query);

        if($stmt->execute($bindParams)) {
            return $stmt;
        }

        return false;
    }

    public function getInsertedId()
    {
        return $this->connection->lastInsertId();
    }

    public function list()
    {
        $query = "SELECT * FROM {$this->table}";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        
        $count = $stmt->rowCount();
        if($count > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $data[] = $row;
            }
        } else {
            $data = [];
        }

        return $data;
    }

    public function find($id)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = :id";
        $bindParams['id'] = $id;
        $stmt = $this->connection->prepare($query);
        $stmt->execute($bindParams);

        if($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $data = $row;
            }
        } else {
            $data = [];
        }
        return $data;
    }

    public function edit($id, $params) 
    {
        $fields = '';
        foreach($params as $key => $param) {
            $fields .= " {$key} = :{$key},";
            $params->$key = htmlspecialchars(strip_tags($param));
            $bindParams[$key] = $param;
        }

        $bindParams['id'] = $id;

        $fields = substr($fields, 0, -1);
        $query = "UPDATE {$this->table} SET {$fields} WHERE id = :id";
        $stmt = $this->connection->prepare($query);

        if($stmt->execute($bindParams)) {
            return true;
        }

        return false;
    }

    public function delete($id) 
    {
        $bindParams['id'] = $id;

        $query = "SELECT * FROM {$this->table} WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $stmt->execute($bindParams);
        if($stmt->rowCount() > 0) {
            $queryDelete = "DELETE FROM {$this->table} WHERE id = :id";
            $stmtDelete = $this->connection->prepare($queryDelete);
            
            if($stmtDelete->execute($bindParams)) {
                return true;
            }
        }
        return false;
    }
}