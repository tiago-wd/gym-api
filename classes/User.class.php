<?php

include_once 'Base.class.php';

class User extends Base
{
    public function __construct($db, $table)
    {
        parent::__construct($db, $table);
    }

    public function create($params)
    {   
        return parent::create($params);
    }

    public function listUsers()
    {
        return parent::list();
    }

    public function findUser($id)
    {
        return parent::find($id);
    }

    public function edit($user_id, $params) 
    {
        return parent::edit($user_id, $params);
    }

    public function delete($id) 
    {
        return parent::delete($id);
    }
}