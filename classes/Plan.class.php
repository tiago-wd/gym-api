<?php

include_once 'Base.class.php';
include_once 'User.class.php';
include_once 'Day.class.php';
include_once 'Exercise.class.php';
include_once 'Mail.class.php';

class Plan extends Base
{
    private $user;
    private $day;
    private $exercise;
    private $mail;

    public function __construct($db, $table)
    {
        parent::__construct($db, $table);
        $this->user = new User($db, 'users');
        $this->day = new Day($db, 'days');
        $this->exercise = new Exercise($db, 'exercises');
        $this->mail = new Mail();
    }

    public function create($params)
    {   
        $plan = parent::create((object) ['plan_name' => $params->plan_name]);
        $plan_id = $this->getInsertedId();
        $this->attachUser($plan_id, $params);
        $this->sendEmail($params->user_id, "Confirmation email", "Assigned to plan");
        $this->attachExercise($plan_id, $params);

        return $plan;
    }

    public function attachUser($plan_id, $params)
    {           
        if(!empty($params->user_id)) {
            $query = "INSERT INTO user_plan (plan_id, user_id) VALUES (:plan_id, :user_id)";
            $stmt = $this->connection->prepare($query);

            foreach($params->user_id as $user_id) {
                $bindParams = [
                    'user_id' => $user_id, 
                    'plan_id' => $plan_id
                ];
                $stmt->execute($bindParams);
            }
            return $stmt;
            
        } 
    }

    public function attachExercise($plan_id, $params)
    {
        if(!empty($params->day)) {
            foreach($params->exercise as $exercise) {
                $paramsDay = [
                    'plan_id' => $plan_id,
                    'name' => $params->day
                ];
                $this->day->create((object) $paramsDay);
                $day_id = $this->getInsertedId();
            }       
        }

        if(!empty($params->exercise)) {

            foreach($params->exercise as $exercise) {
                $paramsExercise = [
                    'day_id' => $day_id,
                    'name' => $exercise
                ];
                $this->exercise->create((object) $paramsExercise);
            }
        }
    }

    public function sendEmail($users_id, $subject, $message)
    {
        foreach($users_id as $user_id) {
            $user = $this->user->find($user_id);
            $this->mail->send($user['email'], $subject, $message);
        }
    }

    public function listPlans()
    {
        $plans = parent::list();
        foreach($plans as $key => $plan) {
            $plans[$key]['users'] = $this->getPlanUsers($plan['id']);
        }
        return $plans;
    }

    public function findPlan($id)
    {
        $plan = parent::find($id);
        $plan['users'] = $this->getPlanUsers($plan['id']);
        $plan['days'] = $this->getPlanDays($plan['id']);
        return $plan;
    }

    public function getPlanUsers($plan_id)
    {
        $query = "SELECT * FROM user_plan up
                    LEFT JOIN users u  ON u.id = up.user_id
                    WHERE up.plan_id = :plan_id";
        $bindParamss['plan_id'] = $plan_id;
        $stmt = $this->connection->prepare($query);
        $stmt->execute($bindParamss);
        
        $count = $stmt->rowCount();
        $users = [];
        if($count > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                array_push($users, [
                    "id" => $id,
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "email" => $email
                ]);
            }
        }

        return $users;
    }
    
    public function getPlanDays($plan_id)
    {
        $query = "SELECT d.id, d.name as day, e.id as exercise_id, e.name as exercise FROM days d
                    LEFT JOIN exercises e ON e.day_id = d.id
                    WHERE d.plan_id = :plan_id";
        $bindParamss['plan_id'] = $plan_id;
        
        $stmt = $this->connection->prepare($query);
        $stmt->execute($bindParamss);
        
        $count = $stmt->rowCount();
        $days = [];
        if($count > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $days[$id]["day"] = $day;
                if($exercise_id) {
                    $days[$id]["exercises"][] = [
                        "id" => $exercise_id,
                        "name" => $exercise
                    ];
                }
            }
        }
        return $days;
    }

    public function edit($plan_id, $params) 
    {
        $plan = parent::edit($plan_id, (object) ['plan_name' => $params->plan_name]);
        $this->attachUser($plan_id, $params);
        $this->attachExercise($plan_id, $params);
        $this->sendEmail($params->user_id, "Notification email", "Changed plan");
        return $plan;
    }

    public function delete($id) 
    {
        return parent::delete($id);
    }
}