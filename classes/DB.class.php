<?php

class DB 
{
    private $host = "localhost";
    private $database = "virtuagym";
    private $username = "root";
    private $password = "";
    public $connection = null;

	public function __construct()
	{
        try{
            $this->connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Error: " . $exception->getMessage();
        }
        return $this->connection;
    }

    public function getDb() {
        if ($this->connection instanceof PDO) {
             return $this->connection;
        }
    }
 
}