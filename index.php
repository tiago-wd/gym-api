<?php include 'inc/header.php'; ?>

<div class="container mt-3">

  <div id="success" class="alert alert-success alert-dismissible fade show" role="alert">
    <span id="messageSuccess"></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div id="danger" class="alert alert-danger alert-dismissible fade show" role="alert">
    <span id="messageDanger"></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
  </div>

  <div id="root"></div>

<div>
<script src="js/app.js"></script>
<script src="js/user.js"></script>
<script src="js/plan.js"></script>
<?php include 'inc/footer.php'; ?>