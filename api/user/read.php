<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../classes/DB.class.php';
include_once '../../classes/User.class.php';

$db = new DB();
$user = new User($db->getDb(), 'users');

if($id = filter_input(INPUT_GET, 'id')) {
    $user = $user->findUser($id);    
    echo json_encode($user);
} else {
    $users = $user->listUsers();
    echo json_encode($users);
}