<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PATCH");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/User.class.php';

$db = new DB();
$user = new User($db->getDb(), 'users');

$params = json_decode(file_get_contents("php://input"));
$user_id = filter_input(INPUT_GET, 'id');

if(!empty($user_id)) {

    if($user->edit($user_id, $params)) {
        echo json_encode(["message" => "User updated successfully."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "User not updated."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "User not updated. Data is missing."));
}

