<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PATCH");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Day.class.php';

$db = new DB();
$day = new Day($db->getDb(), 'days');

$params = json_decode(file_get_contents("php://input"));
$day_id = filter_input(INPUT_GET, 'id');

if(!empty($day_id)) {

    if($day->edit($day_id, $params)) {
        echo json_encode(["message" => "Day updated successfully."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Day not updated."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Day not updated. Data is missing."));
}

