<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../classes/DB.class.php';
include_once '../../classes/Day.class.php';

$db = new DB();
$day = new Day($db->getDb(), 'days');

if($id = filter_input(INPUT_GET, 'id')) {
    $day = $day->findDay($id);    
    echo json_encode($day);
} else {
    $days = $day->listDays();
    echo json_encode($days);
}