<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PATCH");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Day.class.php';

$db = new DB();
$day = new Day($db->getDb(), 'days');

$day_id = filter_input(INPUT_GET, 'id');

if(!empty($day_id)) {
    
    if($day->delete($day_id)) {
        echo json_encode(["message" => "Day deleted successfully."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Day not deleted."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Day not deleted. Day ID is missing."));
}

