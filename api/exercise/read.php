<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../classes/DB.class.php';
include_once '../../classes/Exercise.class.php';

$db = new DB();
$exercise = new Exercise($db->getDb(), 'exercises');

if($id = filter_input(INPUT_GET, 'id')) {
    $exercise = $exercise->findExercise($id);    
    echo json_encode($exercise);
} else {
    $exercises = $exercise->listExercises();
    echo json_encode($exercises);
}