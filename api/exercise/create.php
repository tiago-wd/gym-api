<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Exercise.class.php';

$db = new DB();
$exercise = new Exercise($db->getDb(), 'exercises');

$params = json_decode(file_get_contents("php://input"));

if(!empty($params->day_id) &&
    !empty($params->name)) {
    
    if($id = $exercise->create($params)) {
        echo json_encode(["message" => $id]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Exercise not registered."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Exercise not registered. Data is missing."));
}

