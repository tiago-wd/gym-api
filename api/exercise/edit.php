<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PATCH");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Exercise.class.php';

$db = new DB();
$exercise = new Exercise($db->getDb(), 'exercises');

$params = json_decode(file_get_contents("php://input"));
$exercise_id = filter_input(INPUT_GET, 'id');

if(!empty($exercise_id)) {

    if($exercise->edit($exercise_id, $params)) {
        echo json_encode(["message" => "Exercise updated successfully."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Exercise not updated."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Exercise not updated. Data is missing."));
}

