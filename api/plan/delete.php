<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PATCH");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Plan.class.php';

$db = new DB();
$plan = new Plan($db->getDb(), 'plans');

$plan_id = filter_input(INPUT_GET, 'id');

if(!empty($plan_id)) {
    
    if($plan->delete($plan_id)) {
        echo json_encode(["message" => "Plan deleted successfully."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Plan not deleted."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Plan not deleted. Plan ID is missing."));
}

