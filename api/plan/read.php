<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../classes/DB.class.php';
include_once '../../classes/Plan.class.php';

$db = new DB();
$plan = new Plan($db->getDb(), 'plans');

if($id = filter_input(INPUT_GET, 'id')) {
    $plan = $plan->findPlan($id);    
    echo json_encode($plan);
} else {
    $plans = $plan->listPlans();
    echo json_encode($plans);
}