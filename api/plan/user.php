<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../classes/DB.class.php';
include_once '../../classes/Plan.class.php';

$db = new DB();
$plan = new Plan($db->getDb());

$params = json_decode(file_get_contents("php://input"));

if(!empty($params->plan_id) &&
    !empty($params->user_id)) {
    //TODO: mudar mensagem
    if($plan->attachUser($params)) {
        echo json_encode(["message" => "Plan successfully registered."]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Plan not registered."]);
    }

} else {
    http_response_code(400);
    echo json_encode(array("message" => "Plan not registered. Data is missing."));
}

